<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

use Cartalyst\Sentry\Users\Eloquent\User as Sentry;

class User extends Sentry implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Table timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public static function owns($user, $var)
	{
		if($user->id == $var->user_id) return true;		

		return false;
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Validate the submitted input
	 *
	 * @param 	Input 	$input
	 * @return 	Validate
	 */
	public static function validateUser($input)
	{
		// Unique
		$rules = array(
			'email'		=>	'required|between:4,24',
			'password'	=>	'required|min:6|confirmed',
			'password_confirmation'	=>	'required|min:6',
		);

		return Validator::make($input, $rules);
	}

	/**
	 * ORM with the [Profile] model
	 *
	 * @return 	mixed
	 */
	public function profile()
	{
		return $this->hasOne('Profile');
	}

	/**
	 * ORM with the [Friends] model
	 *
	 * @return 	mixed
	 */
	public function friends()
	{
		return $this->hasMany('Friend');
	}

	public function messages()
	{
		return $this->hasMany('User');
	}

	public function posts()
	{
		return $this->hasMany('Post');
	}
}