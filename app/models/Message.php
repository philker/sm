<?php

class Message extends Eloquent {
	
	/**
	 * Table timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'messages';

	/**
	 * Columns fillable by this model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'user_id', 'recepient_id',
		'body', 'attachment'
	);

	public function user()
	{
		return $this->belongsTo('user');
	}

	/**
	 *
	 *
	 *
	 */
	public static function sentBox()
	{
		$user = Sentry::getUser();
		$sentbox = self::where('user_id', '=', $user->id)
			->get();

		return $sentbox;
	}

	/**
	 *
	 *
	 */
	public static function inbox()
	{
		$user = Sentry::getUser();
		$inbox = self::where('recepient_id', '=', $user->id)
			->get();

		return $inbox;
	}

	/**
	 *
	 *
	 */
	public function preview()
	{
		if(count(($body = $this->body)) > 50) {
			return substr($body, 0, 50);
		}

		return $this->body;
	}

	public static function countPreview($inbox)
	{
		$count = count($inbox);
		if($count < 1) {
			return 'You have no messages in your inbox';
		} else if($count >= 1) {
			$string = ($count == 1)
				? 'message'
				: 'messages';
		}

		return $count . ' ' . $string;
	}

	public function date()
	{
		$carbon = date('M j, Y', strtotime($this->created_at));

		return $carbon;
	}

	public function attachment()
	{
		return url('uploads') . '/' . $this->attachment;
	}
}