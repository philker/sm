<?php

class Friend extends Eloquent {
	
	/**
	 * Table timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'friends';

	/**
	 * Columns fillable by this model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'user_id', 'friend_id'
	);

	public static function add($user)
	{
		$friend = new Friend(array(
			'user_id'	=>	Sentry::getUser()->id,
			'friend_id'	=>	$user->id
		));

		$o = new Friend(array(
			'user_id'	=>	$user->id,
			'friend_id'	=>	Sentry::getUser()->id
		));

		if($friend->save() && $o->save()) {
			return true;
		}

		return false;
	}

	public function count()
	{
		return count($this->$user->friends);
	}

	public static function already($user)
	{
		$auth = Sentry::getUser()->id;
		if($user->id == $auth) return false;

		$res = self::where('user_id', '=', $auth)
			->where('friend_id', '=', $user->id)
			->orWhere(function($query) use($user, $auth)
			{
				$query->where('user_id', '=', $user->id)
					->where('friend_id', '=', $auth);
			})
			->get();

		return (count($res) !== 0) 
			? true
			: false;
	}

	public static function mutualConnection($user)
	{
		$auth = Sentry::getUser();
		$f = $user->friends;
		$mutual = [];

		foreach($f as $d) {
			$fs = self::where('friend_id', '=', $auth->id)
			->where('user_id', '=', $d->friend_id)
			->first();

			array_push($mutual, $fs);
		}

		return $mutual;
	}

	/**
	 * ORM with the [Friends] model
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('user');
	}

	public static function grabAll($user)
	{
		$friends = self::where('friend_id', '=', $user->id)
			->where('user_id', '!=', Sentry::getUser()->id)
			->take(6)
			->get();

		$users = [];

		foreach($friends as $friend) {
			array_push($users, Sentry::findUserById($friend->user->id));
		}

		return $users;
	}
}