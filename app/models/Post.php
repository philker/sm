<?php

class Post extends Eloquent {
	
	/**
	 * Table timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

	/**
	 * Columns fillable by this model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'user_id', 'body', 'photo'
	);

	public function user()
	{
		return $this->belongsTo('user');
	}

	public static function feed()
	{
		$auth = Sentry::getUser();
		$friends = Friend::where('friend_id', '=', $auth->id)
			->get();

		$ret = [];

		foreach($friends as $friend) {
			array_push($ret, self::where('user_id', $friend->user_id)->first());
		}

		$own = self::where('user_id', $auth->id)->get();

		foreach($own as $ox) {
			array_push($ret, $ox);
		}

		return $ret;
	}

	public function photo()
	{
		return url('uploads') . '/' . $this->photo;
	}
}