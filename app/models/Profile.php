<?php

class Profile extends Eloquent {

	/**
	 * Table timestamps
	 *
	 * @var boolean
	 */
	public $timestamps = true;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'profiles';

	/**
	 * Columns fillable by this model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'user_id',
		'first_name',
		'last_name',
		'birthdate',
		'gender',
		'description',
		'avatar',
		'bg'
	);

	public function avatar()
	{
		return asset('uploads') . '/' . $this->avatar;
	}

	public function uploadAvatar($image, $profile)
	{
		$file = $image->getClientOriginalName();
		$upload = $image->move(public_path() . '/uploads/', $file);

		if($upload)
			$profile->avatar = $file;
			if($profile->save()) return true;

		return false;
	}


	public function uploadBG($image, $profile)
	{
		$file = $image->getClientOriginalName();
		$upload = $image->move(public_path() . '/uploads/', $file);
		if($upload)
			$profile->bg = $file;
			if($profile->save()) return true;

		return false;
	}

	public function greet()
	{
		return (is_null($fn = $this->first_name))
			? $this->$user->username
			: $fn;
	}


	public function validate()
	{
		//
	}

	public function bg()
	{
		return asset('uploads') . '/' . $this->bg;
	}

	public function birthday()
	{
		$date = new DateTime($this->birthdate);
		return $date->format('F j, Y');
	}

	/**
	 *
	 *
	 *
	 */
	public function birthdate($date)
	{
		$date = new DateTime($date);
		return $date->format($date);
	}

	/**
	 * ORM with the [User] model
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}