<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersSeeder');
		$this->command->info('[Users] table seeded!');
		$this->call('ProfilesSeeder');
		$this->command->info('[Profiles] table seeded!');
		$this->call('FriendsSeeder');
		$this->command->info('[Friends] table seeded!');
		$this->call('MessageSeeder');
		$this->command->info('[Messages] table seeded!');
		$this->call('PostSeeder');
		$this->command->info('[Posts] table seeded!');
	}

}