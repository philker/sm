<?php

class FriendsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('friends');
		$db->delete();

		$users = array(
			array(
				'id'			=>	1,
				'user_id'		=>	1,
				'friend_id'		=>	2

			),

			array(
				'id'			=>	2,
				'user_id'		=>	1,
				'friend_id'		=>	3
			),

			array(
				'id'			=>	3,
				'user_id'		=>	2,
				'friend_id'		=>	1
			),

			array(
				'id'			=>	4,
				'user_id'		=>	3,
				'friend_id'		=>  1,
			)
		);

		$db->insert($users);
	}

}