<?php

class MessageSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('messages');
		$db->delete();

		$users = array(
			array(
				'id'			=>	1,
				'user_id'		=>	1,
				'recepient_id'	=>	2,
				'body'			=>	'Lipsum yeah!'

			),

			array(
				'id'			=>	2,
				'user_id'		=>	1,
				'recepient_id'	=>	2,
				'body'			=>	'Lipsum yeah!'
			),

			array(
				'id'			=>	3,
				'user_id'		=>	2,
				'recepient_id'	=>	1,
				'body'			=>	'Lipsum yeah!'
			)
		);

		$db->insert($users);
	}

}