<?php

class UsersSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('users');
		$db->delete();

		$users = array(
			array(
				'id'			=>	1,
				'password'		=>	'pass',
				'email'			=>	'test@one.com',
				'activated'		=>	1

			),

			array(
				'id'			=>	2,
				'password'		=>	'pass',
				'email'			=>	'test@two.com',
				'activated'		=>	1
			),

			array(
				'id'			=>	3,
				'password'		=>	'pass',
				'email'			=>	'test@three.com',
				'activated'		=>	1
			)
		);

		foreach($users as $user) {
			Sentry::register($user);
		}
	}

}