<?php

class ProfilesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('profiles');
		$db->delete();

		$profiles = array(
			array(
				'id'			=>	1,
				'user_id'		=>	1,
				'first_name'	=>	'Kier',
				'last_name'		=>	'Pogi',
				'gender'		=>	'M',
				'birthdate'		=>	strtotime('November 23, 1996'),
				'description'	=>	'Napakapogi ko lang, at hindi ko alam kung bakit po.',
				'avatar'		=>	'avatar.png',
			),

			array(
				'id'			=>	2,
				'user_id'		=>	2,
				'first_name'	=>	'Dave',
				'last_name'		=>	'Pogi',
				'gender'		=>	'M',
				'birthdate'		=>	strtotime('June 9, 2010'),
				'description'	=>	'Napakapogi ko lang, at hindi ko alam kung bakit po.',
				'avatar'		=>	'avatar.png',
			),

			array(
				'id'			=>	3,
				'user_id'		=>	3,
				'first_name'	=>	'JC',
				'last_name'		=>	'Pogi',
				'gender'		=>	'F',
				'birthdate'		=>	strtotime('November 5, 1990'),
				'description'	=>	'Napakapogi ko lang, at hindi ko alam kung bakit po.',
				'avatar'		=>	'avatar.png',
			)
		);

		$db->insert($profiles);
	}

}