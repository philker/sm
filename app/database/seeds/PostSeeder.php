<?php

class PostSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$db = DB::table('posts');
		$db->delete();

		$posts = array(
			array(
				'id'			=>	1,
				'user_id'		=>	1,
				'body'			=>	'Heyheyheyhey today here\'s my status!!'

			),

			array(
				'id'			=>	2,
				'user_id'		=>	1,
				'body'			=>	'Heyheyheyhey today here\'s my status!!'
			),

			array(
				'id'			=>	3,
				'user_id'		=>	2,
				'body'			=>	'Heyheyheyhey today here\'s my status!!'
			),

			array(
				'id'			=>	4,
				'user_id'		=>	3,
				'body'			=>	'Heyheyheyhey today here\'s my status!!'
			)
		);

		$db->insert($posts);
	}

}