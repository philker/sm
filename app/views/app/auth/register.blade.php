@extends('template.master')

@section('content')
	<div class="panel panel-default mini">
		<div class="panel-heading">
			Create an account
		</div>
		<div class="panel-body">
			@include('template/modules/alerts.alert')
			{{ Form::open(array('url' => 'auth/register')) }}
				{{ Form::token() }}

				<div class="form-group">
					<label>
						@include('template/modules.required')
						Email
					</label>
					{{ Form::email('email',
						Input::old('email'),
						array('class' => 'form-control')) }}
				</div>
				@if($errors->first('email'))
					<div class="alert alert-danger">
						{{ $errors->first('email') }}
					</div>
				@endif

				<div class="form-group">
					<label>
						@include('template/modules.required')
						Password
					</label>
					{{ Form::password('password',
						array('class' => 'form-control')) }}
				</div>
				@if($errors->first('password'))
					<div class="alert alert-danger">
						{{ $errors->first('password') }}
					</div>
				@endif

				<div class="form-group">
					<label>
						@include('template/modules.required')
						Confirm Password
					</label>
					{{ Form::password('password_confirmation',
						array('class' => 'form-control')) }}
				</div>
				@if($errors->first('password_confirmation'))
					<div class="alert alert-danger">
						{{ $errors->first('password_confirmation') }}
					</div>
				@endif

				<hr>

				<div class="form-group">
					<label>First Name</label>
					{{ Form::text('first_name',
						Input::old('first_name'),
						array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					<label>Last Name</label>
					{{ Form::text('last_name',
						Input::old('last_name'),
						array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					{{ Form::submit('Register',
					array('class' => 'btn btn-success btn-block'))}}
				</div>
			{{ Form::close() }}
		</div>
	</div>
@stop