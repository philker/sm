@extends('template.master')

@section('content')
	<div class="panel panel-default mini">
		<div class="panel-heading">
			Login
		</div>
		<div class="panel-body">
			@include('template/modules/alerts.alert')
			{{ Form::open(array('url' => 'auth/login')) }}
				{{ Form::token() }}

				<div class="form-group">
					<label>Email</label>
					{{ Form::email('email',
						Input::old('email'),
						array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					<label>Password</label>
					{{ Form::password('password',
						array('class' => 'form-control')) }}
				</div>

				<div class="form-group">
					{{ Form::submit('Login!',
					array('class' => 'btn btn-success btn-block'))}}
				</div>
			{{ Form::close() }}
		</div>
	</div>
@stop