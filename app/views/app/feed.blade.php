@extends('template.master')

@section('content')
	<div class="sidebar">
		<div class="nav nav-list">
			<li class="active open">
				<a href="#" class="dropdown-toggle">
					<i class="icon-paperclip"></i>
					<span class="menu-text">
						News Feed
					</span>
					<b class="arrow icon-angle-down"></b>
				</a>

				<ul class="submenu">
					<li class="active">
						<a href="#">
							<i class="icon-double-angle-right"></i>
							Recent Posts
						</a>
					</li>

					<li>
						<a href="#">
							Top Stories
						</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{ URL::to('h/message') }}">
					<i class="icon-comments"></i>
					<span class="menu-text">
						Messages
					</span>
				</a>
			</li>
			<li>
				<a href="#">
					<i class="icon-calendar"></i>
					<span class="menu-text">
						Events
					</span>
				</a>
			</li>
		</div>
	</div>

	<div class="main-content">
		<div class="page-content">
			<div class="page-header">
				<h1>
					Newsfeed
					<small>
						<i class="icon-double-angle-right"></i>
						Recent posts by your friends
					</small>
				</h1>
			</div>

			<div class="space-6"></div>
			{{ Form::open(array('url' => 'h/post/new', 'files' => true)) }}
				{{ Form::token() }}
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="text-center"> Post a new status </h5>
					</div>

					<div class="panel-body">
						<div class="form-group">
							{{ Form::textarea('body', Input::old('body'), array('class' => 'form-control', 'rows' => 5)) }}
						</div>
					</div>

					<div class="panel-footer">
						<div class="row">							
							<div class="col-md-6">
								<input type="file" name="photo" class="form-control">
							</div>

							<div class="col-md-6">
								<button type="submit" class="btn btn-success pull-right">Post Status</button>
							</div>
						</div>
					</div>
				</div>
			{{ Form::close() }}


			<div class="space-20"></div>
			<div class="widget-box transparent">
				<div class="widget-header widget-header-small">
					<h4 class="blue smaller">
						<i class="icon-rss orange"></i>
						Recent Activities
					</h4>
						<div class="widget-toolbar action-buttons">
						<a href="#" data-action="reload">
							<i class="icon-refresh blue"></i>
						</a>
						&nbsp;
						<a href="#" class="pink">
							<i class="icon-trash"></i>
						</a>
					</div>
				</div>

				<div class="widget-body">
					<div class="widget-main padding-8">
						<div id="profile-feed-1" class="profile-feed">
							@foreach($feed as $data)
								@if($data->photo)
									<br />
									<img src="{{ $data->photo() }}" class="img-rounded" width="200" height="200">
									<br />
									<a href="{{ $data->photo() }}">
										<small> <i class="glyphicon glyphicon-zoom-in"></i> Maximize </small>
									</a>
								@endif
								<div class="profile-activity clearfix">
									<div>
										<img class="pull-left" alt="Avatar" src="{{ $data->user->profile->avatar() }}" />
										<a class="user" href="#"> {{ $data->user->profile->first_name }}&nbsp;{{ $data->user->profile->last_name }}</a>
										{{ $data->body }}
										<div class="time">
											<i class="icon-time bigger-110"></i>
												{{ date('M j, Y', strtotime($data->created_at)) }}
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop