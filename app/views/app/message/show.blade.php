@extends('template.master')

@section('content')
	<div class="col-xs-12">
		<div class="page-content">
			<h3> Content </h3>
			<hr>
			<p>{{ $message->body}}</p>

			<hr>
			<div class="clearfix">
				<p class="pull-left">
					<a href="#">
						<small> {{ $message->user->email }} </small> <br />
					</a>
					<i> Sender </i>
				</p>

				<p class="pull-right">
					<small>
						@if($message->attachment)
							<a href="{{ $message->attachment() }}">
								Attachment <i class="glyphicon glyphicon-cloud"></i>
							</a>
						@endif
					</small> <br />
					<small> {{ $message->date() }} </small> <br />
					<i> Date Sent </i>
				</p>
			</div>
			<p>
				<a href="{{ URL::to('h/message') }}" class="btn btn-info btn-block">
					<i class="icon-share-alt"></i> Back to inbox
				</a>
			</p>
		</div>
	</div>
@stop