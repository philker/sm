@extends('template.master')

@section('content')
	<div class="col-xs-12">
		<div class="page-content">
		<p>
			<a href="{{ URL::to('h/message') }}" class="btn btn-info">
				<i class="icon-share-alt"></i> Back to inbox
			</a>
		</p>
	{{ Form::open(array('files' => true)) }}
		{{ Form::token() }}

		<div class="form-group">
			<label> To (Recepient Email)</label>
			{{ Form::text('email', Input::old('email'), array('class' => 'form-control')) }}
		</div>

		<div class="form-group">
			<label> Message </label>
			{{ Form::textarea('body', Input::old('body'), array('class' => "form-control", 'rows' => "12")) }}
		</div>

		<div class="form-group">
			<label> Attachment </label>
			<input type="file" name="attachment" class="form-control">
		</div>

		<div class="form-group">
			{{ Form::submit('Send Message', array('class' => 'btn btn-success')) }}
		</div>
	{{ Form::close() }}
		</div>
	</div>
@stop