@extends('template.master')

@section('content')
						<div class="row">

							<div class="col-xs-12">
								<div class="page-content">
								<!-- PAGE CONTENT BEGINS -->



								<div class="row">

									<div class="col-xs-12">

										<div class="tabbable">

											<ul id="inbox-tabs" class="inbox-tabs nav nav-tabs padding-16 tab-size-bigger tab-space-1">
												<li class="li-new-mail pull-right">
													<a href="{{ URL::to('h/message/new') }}" class="btn btn-small btn-purple no-border">
														<i class=" icon-envelope bigger-130"></i>
														<span class="bigger-110">Write Mail</span>
													</a>
												</li><!-- ./li-new-mail -->



												<li class="active">

													<a data-toggle="tab" href="#inbox" data-target="inbox">

														<i class="blue icon-inbox bigger-130"></i>

														<span class="bigger-110">Inbox</span>

													</a>

												</li>



												<li>

													<a data-toggle="tab" href="#sent" data-target="sent">

														<i class="orange icon-location-arrow bigger-130 "></i>

														<span class="bigger-110">Sent</span>

													</a>

												</li>



												

											</ul>



											<div class="tab-content no-border no-padding">

												<div class="tab-pane in active">

													<div class="message-container">

														<div id="id-message-list-navbar" class="message-navbar align-center clearfix">

															<div class="message-bar">

																<div class="message-infobar" id="id-message-infobar">

																	<span class="blue bigger-150">Inbox</span>

																</div>



																<div class="message-toolbar hide">

																	<div class="inline position-relative align-left">

																		<a href="#" class="btn-message btn btn-xs dropdown-toggle" data-toggle="dropdown">

																			<span class="bigger-110">Action</span>



																			<i class="icon-caret-down icon-on-right"></i>

																		</a>



																		<ul class="dropdown-menu dropdown-lighter dropdown-caret dropdown-125">

																			<li>

																				<a href="#">

																					<i class="icon-mail-reply blue"></i>

																					&nbsp; Reply

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-mail-forward green"></i>

																					&nbsp; Forward

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-folder-open orange"></i>

																					&nbsp; Archive

																				</a>

																			</li>



																			<li class="divider"></li>



																			<li>

																				<a href="#">

																					<i class="icon-eye-open blue"></i>

																					&nbsp; Mark as read

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-eye-close green"></i>

																					&nbsp; Mark unread

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-flag-alt red"></i>

																					&nbsp; Flag

																				</a>

																			</li>



																			<li class="divider"></li>



																			<li>

																				<a href="#">

																					<i class="icon-trash red bigger-110"></i>

																					&nbsp; Delete

																				</a>

																			</li>

																		</ul>

																	</div>



																	<div class="inline position-relative align-left">

																		<a href="#" class="btn-message btn btn-xs dropdown-toggle" data-toggle="dropdown">

																			<i class="icon-folder-close-alt bigger-110"></i>

																			<span class="bigger-110">Move to</span>



																			<i class="icon-caret-down icon-on-right"></i>

																		</a>



																		<ul class="dropdown-menu dropdown-lighter dropdown-caret dropdown-125">

																			<li>

																				<a href="#">

																					<i class="icon-stop pink2"></i>

																					&nbsp; Tag#1

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-stop blue"></i>

																					&nbsp; Family

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-stop green"></i>

																					&nbsp; Friends

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-stop grey"></i>

																					&nbsp; Work

																				</a>

																			</li>

																		</ul>

																	</div>



																	<a href="#" class="btn btn-xs btn-message">

																		<i class="icon-trash bigger-125"></i>

																		<span class="bigger-110">Delete</span>

																	</a>

																</div>

															</div>


															</div>

														</div>



														<div id="id-message-item-navbar" class="hide message-navbar align-center clearfix">

															<div class="message-bar">

																<div class="message-toolbar">

																	<div class="inline position-relative align-left">

																		<a href="#" class="btn-message btn btn-xs dropdown-toggle" data-toggle="dropdown">

																			<span class="bigger-110">Action</span>



																			<i class="icon-caret-down icon-on-right"></i>

																		</a>



																		<ul class="dropdown-menu dropdown-lighter dropdown-caret dropdown-125">

																			<li>

																				<a href="#">

																					<i class="icon-mail-reply blue"></i>

																					&nbsp; Reply

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-mail-forward green"></i>

																					&nbsp; Forward

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-folder-open orange"></i>

																					&nbsp; Archive

																				</a>

																			</li>



																			<li class="divider"></li>



																			<li>

																				<a href="#">

																					<i class="icon-eye-open blue"></i>

																					&nbsp; Mark as read

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-eye-close green"></i>

																					&nbsp; Mark unread

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-flag-alt red"></i>

																					&nbsp; Flag

																				</a>

																			</li>



																			<li class="divider"></li>



																			<li>

																				<a href="#">

																					<i class="icon-trash red bigger-110"></i>

																					&nbsp; Delete

																				</a>

																			</li>

																		</ul>

																	</div>



																	<div class="inline position-relative align-left">

																		<a href="#" class="btn-message btn btn-xs dropdown-toggle" data-toggle="dropdown">

																			<i class="icon-folder-close-alt bigger-110"></i>

																			<span class="bigger-110">Move to</span>



																			<i class="icon-caret-down icon-on-right"></i>

																		</a>



																		<ul class="dropdown-menu dropdown-lighter dropdown-caret dropdown-125">

																			<li>

																				<a href="#">

																					<i class="icon-stop pink2"></i>

																					&nbsp; Tag#1

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-stop blue"></i>

																					&nbsp; Family

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-stop green"></i>

																					&nbsp; Friends

																				</a>

																			</li>



																			<li>

																				<a href="#">

																					<i class="icon-stop grey"></i>

																					&nbsp; Work

																				</a>

																			</li>

																		</ul>

																	</div>



																	<a href="#" class="btn btn-xs btn-message">

																		<i class="icon-trash bigger-125"></i>

																		<span class="bigger-110">Delete</span>

																	</a>

																</div>

															</div>



															<div>

																<div class="messagebar-item-left">

																	<a href="#" class="btn-back-message-list">

																		<i class="icon-arrow-left blue bigger-110 middle"></i>

																		<b class="bigger-110 middle">Back</b>

																	</a>

																</div>



																<div class="messagebar-item-right">

																	<i class="icon-time bigger-110 orange middle"></i>

																	<span class="time grey">Today, 7:15 pm</span>

																</div>

															</div>

														</div>



														<div id="id-message-new-navbar" class="hide message-navbar align-center clearfix">

															<div class="message-bar">

																<div class="message-toolbar">

																	<a href="#" class="btn btn-xs btn-message">

																		<i class="icon-save bigger-125"></i>

																		<span class="bigger-110">Save Draft</span>

																	</a>



																	<a href="#" class="btn btn-xs btn-message">

																		<i class="icon-remove bigger-125"></i>

																		<span class="bigger-110">Discard</span>

																	</a>

																</div>

															</div>



															<div class="message-item-bar">

																<div class="messagebar-item-left">

																	<a href="#" class="btn-back-message-list no-hover-underline">

																		<i class="icon-arrow-left blue bigger-110 middle"></i>

																		<b class="middle bigger-110">Back</b>

																	</a>

																</div>



																<div class="messagebar-item-right">

																	<span class="inline btn-send-message">

																		<button type="button" class="btn btn-sm btn-primary no-border">

																			<span class="bigger-110">Send</span>



																			<i class="icon-arrow-right icon-on-right"></i>

																		</button>

																	</span>

																</div>

															</div>

														</div>



														<div class="message-list-container">

															<div class="message-list" id="message-list">

															@foreach($inbox as $message)

																<div class="message-item">

																	<label class="inline">
																		<a href="#">
																			<i class="icon-trash red message-delete" data-id="{{ $message->id }}"></i>
																		</a>
																		<span class="lbl"></span>
																	</label>

																	<span class="sender">{{ $message->user->email }} </span>

																	<span class="time">{{ $message->date() }}</span>



																	<span class="summary">

																		<span class="message-flags">

																			<i class="icon-mail-forward light-grey"></i>

																		</span>
																		
																		<a href="{{ URL::to('h/message' . '/show/' . $message->id)  }}">
																			<span class="text">
																				{{ $message->preview() }}
																			</span>
																		</a>

																	</span>

																</div>
															@endforeach

															</div>

														</div><!-- /.message-list-container -->



														<div class="message-footer clearfix">
															<div class="pull-left"> {{ Message::countPreview($inbox) }} </div>


													</div><!-- /.message-container -->

												</div><!-- /.tab-pane -->

											</div><!-- /.tab-content -->

										</div><!-- /.tabbable -->

										</div>

									</div><!-- /.col -->

								</div><!-- /.row -->

@stop

@section('script')
	<script>
		$('.message-delete').on('click', function(e)
		{
			var self = $(this);
			e.preventDefault();
			$.ajax({
				url: '{{ URL::to('h/message/delete') }}' + '/' + self.data('id'),
				type: 'DELETE',
				dataType: 'json',
				success: function(data) {
					location.reload();
				}
			});
		});
	</script>
@stop