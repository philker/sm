@extends('template.master')

@section('content')
	<div class="page-content">
		<form method="GET" action="{{ URL::to('h/search') }}">
			<div class="form-group">
				<label> User: </label>
				<div class="input-group">
					<input type="text" name="q" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-sm btn-success">
							<i class="icon-search bigger-110"></i>
						</button>
					</span>
				</div>
			</div>
		</form>

		

		@if(!empty($profiles))
			@foreach($profiles as $profile)
				<hr>
				<div class="row">
					<div class="col-xs-3">
						<img src="{{ $profile->avatar() }}" width="189" height="189">
					</div>

					<div class="col-xs-9">
						<h2> &nbsp; &nbsp;{{ $profile->first_name }}&nbsp;{{ $profile->last_name}} </h2>
						&nbsp; &nbsp;&nbsp; &nbsp;
						<a href="{{ URL::to('h/user/' . $profile->user_id . '/profile') }}" type="button" class="btn btn-primary btn-xs">
							<i class="icon-link"></i> Visit Profile
						</a>
					</div>
				</div>

				<div class="space-6"></div>
			@endforeach
		@endif

		@if(!empty($profiles) && count($profiles) == 0)
			<h5> No Results for {{ $q }} </h5>
		@endif
	</div>
@stop