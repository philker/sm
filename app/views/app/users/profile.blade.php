@extends('template.master')

@section('style')
	@if(!is_null($profile->bg))
		<style>
			body {
				background-image: url('{{ $profile->bg() }}');
			}

			.panel-body {
				padding: 0;
				margin: 0;
			}
		</style>
	@endif
@stop

@section('content')
	<div class="col-xs-12">
		<div class="page-content">
			@include('template/modules/alerts.alert')
			<div class="user-profile row">
				<div class="col-xs-12 col-sm-3 center">
					<div>
						<span class="profile-picture">
							<img id="avatar"
							class="img-responsive"
							alt="Profile Picture"
							src="{{ $profile->avatar() }}"
							width="182"
							height="202">
						</span>

						<div class="space-4"></div>

						<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
							<div class="inline position-relative">
								<span class="white"> {{ $profile->first_name }}&nbsp;{{ $profile->last_name }} </span>
							</div>
						</div>

						@if(Sentry::check())
							@if(!User::owns(Sentry::getUser(), $profile))
								<div class="space-6">
								</div>

								<div class="profile-contact-info">
									<div class="profile-contact-links align-left">
										@if(!Friend::already($user))
											<a class="btn btn-link"
												href="{{ URL::route('app.user.add', $user->id) }}">
												<i class="icon-plus-sign bigger-120 green"></i>
												Add as Friend
											</a>
										@endif

										<a class="btn btn-link"
											href="#">
											<i class="icon-coffee pink"></i>
											Poke
										</a>
									</div>
								</div>

								<div class="space-4"></div>
								<div class="hr hr12 dotted"></div>

								<div class="clearfix">
									<div class="grid2">
										<span class="bigger-175 blue">
											{{ $friends->count() }}
										</span> <br />
										Friends
									</div>

									
									<div class="grid2">
										<span class="bigger-175 blue">
											{{ count(Friend::mutualConnection($user))-1 }}
										</span> <br />
										Mutual
									</div>
								</div>
								@if(count(Friend::grabAll($user)) >= 1)
									<div class="space-6"></div>
									<div class="panel panel-default">
										<div class="panel-heading">
											Friends
										</div>
										<div class="panel-body">
											<div class="row">
												@foreach(Friend::grabAll($user) as $index => $fuser)
													<div class="col-xs-6">
														<a href="{{ URL::to('h/user/' . $fuser->profile->user->id . '/profile') }}">
															<img src="{{ $fuser->profile->avatar() }}" class="img-rounded">
														</a> <br />
														<small> {{ $fuser->profile->first_name }} </small>
													</div>
													@if(($index + 1) % 2 == 0)
														<br />
													@endif
												@endforeach
											</div>
										</div>
									</div>
								@endif
							@else
								<p>
									<span class="bigger-175 blue">
										{{ $friends->count() }}
									</span> <br />
									Friends
								</p>
							@endif
						@endif
					</div>
				</div>

				<div class="col-xs-12 col-sm-9">
					<div class="profile-user-info profile-user-info-striped">
						<div class="profile-info-row">
							<div class="profile-info-name"> Name </div>
							<div class="profile-info-value">
								<span>{{ $profile->first_name }}&nbsp;{{ $profile->last_name }}</span>
							</div>
						</div>

						<div class="profile-info-row">
							<div class="profile-info-name"> Birthdate</div>

							<div class="profile-info-value">
								<span>{{ $profile->birthday() }}</span>
							</div>
						</div>

						<div class="profile-info-row">
							<div class="profile-info-name"> About</div>
							<div class="profile-info-value">
								<span>{{ $profile->description }}</span>
							</div>
						</div>
					</div><!-- /span -->

					<div class="space-20"></div>
					<div class="widget-box transparent">
						<div class="widget-header widget-header-small">
							<h4 class="blue smaller">
								<i class="icon-rss orange"></i>
								Recent Activities
							</h4>
								<div class="widget-toolbar action-buttons">
								<a href="#" data-action="reload">
									<i class="icon-refresh blue"></i>
								</a>
								&nbsp;
								<a href="#" class="pink">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>

					<div class="widget-body">
						<div class="widget-main padding-8">
							<div id="profile-feed-1" class="profile-feed">
								@foreach($posts as $post)
									@if($post->photo)
										<br />
										<img src="{{ $post->photo() }}" class="img-rounded" width="200" height="200">
										<br />
										<a href="{{ $post->photo() }}">
											<small> <i class="glyphicon glyphicon-zoom-in"></i> Maximize </small>
										</a>
									@endif
									<div class="profile-activity clearfix">
										<div>
											<img class="pull-left" alt="Avatar" src="{{ $profile->avatar() }}" />
											<a class="user" href="#"> {{ $profile->first_name }}&nbsp;{{ $profile->last_name }}</a>
											{{ $post->body }}
											<div class="time">
												<i class="icon-time bigger-110"></i>
													an hour ago
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>

				</div>			
			</div>
		</div>
	</div>
@stop