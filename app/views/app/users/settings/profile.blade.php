@extends('template.master')

@section('content')
	<div class="sidebar">
		<div class="nav nav-list">
			<li class="active open">
				<a href="#" class="dropdown-toggle">
					<i class="icon-paperclip"></i>
					<span class="menu-text">
						Settings
					</span>
					<b class="arrow icon-angle-down"></b>
				</a>

				<ul class="submenu">
					<li>
						<a href="{{ URL::to('h/user/settings') }}">
							General Account Settings
						</a>
					</li>

					<li class="active">
						<a href="#">
						<i class="icon-double-angle-right"></i>
							Profile Settings
						</a>
					</li>
				</ul>
			</li>
		</div>
	</div>

	<div class="main-content">
		<div class="page-content">
			<div class="page-header">
				<h1> Profile Settings </h1>
			</div>

			@include('template/modules/alerts.alert')
				{{ Form::open(array('url' => 'h/user/profile/settings', 'files' => true)) }}
					{{ Form::token() }}

					<div class="form-group">
						<label>First Name</label>
						{{ Form::text('first_name',
							$profile->first_name,
							array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label>Last Name</label>
						{{ Form::text('last_name',
							$profile->last_name,
							array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label>Birthdate</label>
						{{ Form::text('birthdate', 
							$profile->birthday(),
							array('id' => 'datepicker', 'class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label>About Me</label>
						{{ Form::textarea('description',
							$profile->description,
							array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label>Avatar</label>
						{{ Form::file('avatar',
							array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label>Custom Background</label>
						{{ Form::file('bg',
							array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						{{ Form::submit('Update Profile',
							array('class' => 'btn btn-success btn-block'))}}
					</div>
				{{ Form::close() }}
		</div>
	</div>
@stop

@section('script')
	<script>
    	$("#datepicker").datepicker("option", "dateFormat", 'MM d yy');
    </script>
@stop