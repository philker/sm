@extends('template.master')

@section('content')
	<div class="sidebar">
		<div class="nav nav-list">
			<li class="active open">
				<a href="#" class="dropdown-toggle">
					<i class="icon-paperclip"></i>
					<span class="menu-text">
						Settings
					</span>
					<b class="arrow icon-angle-down"></b>
				</a>

				<ul class="submenu">
					<li class="active">
						<a href="#">
							General Account Settings
						</a>
					</li>

					<li>
						<a href="{{ URL::to('h/user/profile/settings') }}">
						<i class="icon-double-angle-right"></i>
							Profile Settings
						</a>
					</li>
				</ul>
			</li>
		</div>
	</div>

	<div class="main-content">
		<div class="page-content">
			<div class="page-header">
				<h1>
					Newsfeed
					<small>
						<i class="icon-double-angle-right"></i>
						Recent posts by your friends
					</small>
				</h1>
			</div>
		</div>
	</div>
@stop