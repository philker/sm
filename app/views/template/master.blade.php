<!DOCTYPE html>
<html>
<head>
	<title> Mini Facebook </title>
	<meta charset="utf-8">
	@yield('style')
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/font-awesome.min.css') }}
	{{ HTML::style('css/colorbox.css') }}
	<!--[if lte IE 7]>
		{{ HTML::style('css/font-awesome-ie7.css') }}
	<![endif]-->

	{{-- page --}}
	{{ HTML::style('css/jquery-ui-1.10.3.custom.min.css') }}
	{{ HTML::style('css/jquery.gritter.css') }}
	{{ HTML::style('css/ace-fonts.css') }}
	{{ HTML::style('css/uncompressed/ace.css') }}
	{{ HTML::style('css/ace.min.css') }}
	{{ HTML::style('css/ace-skins.min.css') }}
	{{ HTML::style('css/chosen.css') }}
	{{ HTML::style('css/jquery-ui-1.10.3.custom.min.css') }}
	{{ HTML::style('css/stylesheet.css') }}

	<!--[if lte IE 8]>
		{{ HTML::style('css/ace-ie.css') }}
	<![endif]-->

	{{ HTML::script('js/ace-extra.min.js') }}

	<!--[if lte IE 9]>
		{{ HTML::script('js/html5shiv.js') }}
		{{ HTML::script('js/respond.min.js') }}
	<![endif]-->
</head>

<body>
	{{-- Navigation --}}
	<div class="navbar navbar-default"
		id="navbar">
		<div class="navbar-container container">
			<div class="navbar-header pull-left">
				<a href="{{ URL::to('h') }}" class="navbar-brand">
					<small>
						Mini Facebook
					</small>
				</a>
			</div>

			<div class="navbar-header pull-right" role="navigation">

				<ul class="nav ace-nav">
					@if(!Sentry::check())
						<li>
							<a href="{{ URL::to('auth/login') }}">
								Login
							</a>
						</li>

						<li>
							<a href="{{ URL::to('auth/register') }}">
								Register
							</a>
						</li>
					@else
						<li>
							<a href="{{ URL::to('h') }}">
								<i class="icon-home"></i>
							</a>
						</li>
						<li>
							<a href="{{ URL::to('h/message') }}">
								<i class="icon-comments-alt"></i>
							</a>
						</li>

						<li>
							<a href="{{ URL::to('h/search') }}">
								<i class="icon-search"></i>
							</a>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown"
								href="#"
								class="dropdown-toggle">
								<img class="nav-user-photo"
									src="{{ Sentry::getUser()->profile->avatar() }}"
									alt="Photo">

								<span class="user-info">
									<small> Welcome, </small>
									{{ Sentry::getUser()->profile->greet() }}
								</span>
								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="{{ URL::route('app.profile', Sentry::getUser()->id) }}">
										<b class="icon-user"></b>
										View Profile
									</a>
								</li>

								<li>
									<a href="{{ URL::route('app.user.settings') }}">
										<b class="icon-lock"></b>
										Account Settings
									</a>
								</li>

								<li>
									<a href="{{ URL::route('app.profile.settings') }}">
										<b class="icon-cog"></b>
										Profile Settings
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="{{ URL::to('auth/logout') }}">
										<b class="icon-cog"></b>
										Logout
									</a>
								</li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
		{{-- Page Content --}}
		@include('template/modules/alerts.alert')
		@yield('content')
	</div>

	{{-- Scripts --}}
	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/jquery-ui-1.10.3.full.min.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/bootbox.min.js') }}
	{{ HTML::script('js/ace-elements.min.js') }}
	{{ HTML::script('js/ace.min.js') }}
	@yield('script')
</body>
</html>