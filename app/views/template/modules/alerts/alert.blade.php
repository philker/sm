<div class="text-center">
	@if(Session::has('success'))
		@include('template/modules/alerts.success')
	@elseif(Session::has('error'))
		@include('template/modules/alerts.error')
	@endif
</div>