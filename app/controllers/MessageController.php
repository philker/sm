<?php

class MessageController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array('getIndex', 'getNew', 'postNew')));
	}

	public function getIndex()
	{
		$inbox = Message::inbox();

		return View::make('app/message.index')
			->with('inbox', $inbox);
	}

	public function getNew()
	{
		return View::make('app/message.create');
	}

	public function postNew()
	{
		$email = Input::get('email');
		$filname = null;
		try {
			if(Input::hasFile('attachment')) {
				$attachment = Input::file('attachment');
				$filename = $attachment->getClientOriginalName();
				$path = public_path() . '\uploads';
				$attachment->move($path, $filename);
			}
			$data = explode(',', $email);
			foreach($data as $datum) {
				$user = Sentry::findUserByLogin(trim($datum));
				if(!empty($user) ||
					!is_null($user)) {
					$message = new Message
					$message->user_id		=	Sentry::getUser()->id;
					$message->recepient_id	=	$user->id;
					$message->body			=	Input::get('body');
					if(Input::hasFile('attachment')) {
						$message->attachment	=	$filename;
					}

					if($message->save()) {
						Session::flash('success', 'Message has been sent');
						return Redirect::to('h/message');
					}
				}
			}
		} catch(Cartalyst\Sentry\Users\UserNotFoundException $e) {
			$error = 'User was not found';
		}

		if(is_null($error) || empty($error))
			$error = 'An error has occured';

		Session::flash('error', $error);
		return Redirect::to(URL::previous())
			->withInput();
	}

	public function getShow($id)
	{
		$message = Message::find($id);

		return View::make('app/message.show')
			->with('message', $message);
	}

	public function deleteDelete($id)
	{
		$message = Message::find($id);
		if(!is_null($message) ||
			!empty($message)) {
			if($message->delete()) {
				Session::flash('success', 'Message deleted!');
				return Response::json(array('status' => true));
			}
		}

		Session::flash('error', 'An error has occured');
		return Response::json(array('status' => error));
	}
}