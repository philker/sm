<?php

class PostController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth', array('only' => array('postStatus')));
	}

	public function postStatus()
	{
		$post = new Post;
		$post->user_id	=	Sentry::getUser()->id;
		$post->body		=	Input::get('body');
		if(Input::hasFile('photo')) {
			$photo = Input::file('photo');
			$filename = $photo->getClientOriginalName();
			$path = public_path() . '\uploads';
			$photo->move($path, $filename);
			$post->photo = $filename;
		}

		if($post->save()) {
			Session::flash('success', 'Status has been posted succesfully');
			return Redirect::to('h');
		}

		Session::flash('error', 'An error has occured');
		return Redirect::to('h')
			->withInput();
	}
}