<?php

class UserController extends BaseController {

	public function __construct()
	{
		$this->beforeFilter('auth', array(
			'except' => array('showProfile')
		));

		$this->beforeFilter('csrf', array(
			'except' => array(
				'showProfile',
				'showProfileSettings',
				'showAccountSettings'
			)
		));
	}

	public function showProfile($id)
	{
		$user = Sentry::findUserById($id);

		return View::make('app/users.profile')
			->with('user', $user)
			->with('profile', $user->profile)
			->with('friends', $user->friends)
			->with('posts', $user->posts);
	}

	public function showProfileSettings()
	{
		$user = Sentry::getUser();

		return View::make('app/users/settings.profile')
			->with('user', $user)
			->with('profile', $user->profile);
	}

	public function updateProfile()
	{
		// I don't see a need for validation
		$profile = Sentry::getUser()->profile;
		$profile->first_name 	= Input::get('first_name');
		$profile->last_name 	= Input::get('last_name');
		$profile->birthdate 	= Input::get('birthdate');
		$profile->gender 		= Input::get('gender');
		$profile->description 	= Input::get('description');

		if($profile->save()) {
			$ia = Input::file('avatar');
			$ib = Input::file('bg');
			$success = true;
			if(Input::hasFile('avatar')) {
				$avatar = $profile->uploadAvatar($ia, $profile);
				if(!$avatar) {
					$success = false;
				}
			}

			if(Input::hasFile('bg')) {
				$bg = $profile->uploadBG($ib, $profile);
				if(!$bg) {
					$success = false;
				}
			}

			if($success) {
				Session::flash('success', 'Your profile has been updated');
				return Redirect::to('h/user/profile/settings');
			}
		}

		Session::flash('error', 'An error has occured');
		return Redirect::to('h/user/profile/settings');
	}

	public function showAccountSettings()
	{
		return View::make('app/users/settings.account');
	}


	public function updateAccount()
	{
		//
	}
}