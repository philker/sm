<?php

class SearchController extends BaseController {

	public function getIndex()
	{
		$q = Input::get('q');

		if(!Input::has('q')) {
			return View::make('app/search.index');
		}

		$profiles = Profile::where('first_name', 'like', '%' .$q)
			->orWhere(function($query) use ($q){
				$query->where('last_name', 'like', '%' . $q . '%');
			})
			->get();

		return View::make('app/search.index')
			->with('profiles', $profiles)
			->with('q', $q);
	}

	public function getSearch($user)
	{

	}
}