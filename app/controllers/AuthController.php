<?php

class AuthController extends BaseController {

	/**
	 *
	 *
	 */
	public function __construct()
	{
		$this->beforeFilter('guest', array(
			'except' => array('getLogout')
		));

		$this->beforeFilter('csrf', array(
			'only' => array('postLogin', 'postRegister'
		)));

		$this->beforeFilter('auth', array(
			'only' => array('getLogout'
		)));
	}

	/**
	 *
	 *
	 *
	 */
	public function getLogin()
	{
		return View::make('app/auth.login');
	}

	/**
	 *
	 *
	 *
	 */
	public function postLogin()
	{
		try {
			$user = array(
				'email'		=>	Input::get('email'),
				'password'	=>	Input::get('password')
			);

			Sentry::authenticate($user, false);
			return Redirect::intended('h');
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			$error = 'Login field is required';
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			$error = 'Password field is required';
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			$error = 'Wrong password, try again';
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			$error = 'User was not found';
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			$error = 'User is not activated';
		}

		Session::flash('error', $error);
		return Redirect::to('auth/login')
			->withInput();
	}

	/**
	 *
	 *
	 *
	 */
	public function getRegister()
	{
		return View::make('app/auth.register');
	}

	/**
	 *
	 *
	 *
	 */
	public function postRegister()
	{
		$validation = User::validateUser(Input::all());

		try {
			if(! $validation->fails() ) {
				echo 'ok';
				$d = array(
					'email'		=>	Input::get('email'),
					'password'	=>	Input::get('password'),
					'activated'	=>	1
				);
				$user = Sentry::register($d);

				echo 'ok2';

				if($user) {
					$l = Sentry::findUserByLogin($d['email']);
					echo 'ok3';
					$profile = new Profile(array(
						'user_id'		=>	$l->id,
						'first_name'	=>	Input::get('first_name'),
						'last_name'		=>	Input::get('last_name')
					));
					
					if($profile->save()) {
						echo 'ok4';
						Session::flash('success',
							'Your account has been registered!');
						Sentry::login($user);
						return Redirect::to('h');
					}
				}
			} 
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			$error = 'Login field is required';
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			$error = 'Password field is required';
		} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
			$error = 'User with this login already exists';
		}

	
		$msg = (is_null($error))
			? 'An error has occured'
			: $error;
		Session::flash('error', $msg);
		return Redirect::to('auth/register')
			->withInput()
			->withErrors($validation);
	}

	public function getLogout()
	{
		if(Sentry::check()) {
			Session::flash('success', 'You have been logged out');
			Sentry::logout();
		}

		return Redirect::to('auth/login');
	}
}