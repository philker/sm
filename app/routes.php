<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * @link /
 * Homepage
 */
Route::get('/', array(
	'as'	=>	'home',
	'uses'	=>	'HomeController@index'
));

/**
 * @link app/*
 * Newsfeed
 */
Route::get('h', array(
	'as'	=>	'app.feed',
	'uses'	=>	'HomeController@feed'
));

/**
 * @link user/{id}/profile
 *
 */
Route::get('h/user/{id}/profile', array(
	'as'	=>	'app.profile',
	'uses'	=>	'UserController@showProfile'
));

Route::get('h/user/settings', array(
	'as'	=>	'app.user.settings',
	'uses'	=>	'UserController@showAccountSettings'
));

Route::post('h/user/settings', array(
	'uses'	=>	'UserController@updateAccountSettings'
));

Route::get('h/user/profile/settings', array(
	'as'	=>	'app.profile.settings',
	'uses'	=>	'UserController@showProfileSettings'
));

Route::post('h/user/profile/settings', array(
	'uses'	=>	'UserController@updateProfile'
));

Route::get('h/user/{id}/add', array(
	'as'	=>	'app.user.add',
	'uses'	=>	'FriendController@addFriend'
));

Route::get('h/search', array(
	'uses'	=>	'SearchController@getIndex'
));

Route::post('h/post/new', 'PostController@postStatus');

/**
 * @link h/message/*
 * Messaging
 */
Route::controller('h/message', 'MessageController');

/**
 * @link auth/*
 * Auth process
 */
Route::controller('auth', 'AuthController');